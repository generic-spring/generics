package generic.spring.generics.base;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.lang.reflect.Field;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@ToString
@MappedSuperclass
public abstract class BaseEntity<T_ID extends Serializable> {

    public static Field ID = null;

    static {
        try {
            ID = (BaseEntity.class).getDeclaredField("id");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected T_ID id;
}