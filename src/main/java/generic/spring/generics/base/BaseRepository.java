package generic.spring.generics.base;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface BaseRepository<
        T extends BaseEntity<T_ID>,
        T_ID extends Serializable
        > extends JpaRepository<T, T_ID> {
}
