package generic.spring.generics.simple;


import generic.spring.generics.Mapper;
import generic.spring.generics.base.BaseEntity;
import generic.spring.generics.types.UpdateEntityDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.function.Supplier;

public class SimpleGenericEntityControllerImpl<
        T extends BaseEntity<T_ID> & Mapper<T_DTO, T>,
        T_DTO extends Mapper<T, T_DTO>,
        T_ID extends Serializable> {

    private final SimpleGenericEntityService<T, T_ID> service;
    private final Supplier<? extends T> ctorEntity;
    private final Supplier<? extends T_DTO> ctorEntityDTO;

    public SimpleGenericEntityControllerImpl(Supplier<? extends T> ctorEntity,
                                             Supplier<? extends T_DTO> ctorEntityDTO,
                                             SimpleGenericEntityService<T, T_ID> service) {
        this.ctorEntity = Objects.requireNonNull(ctorEntity);
        this.ctorEntityDTO = Objects.requireNonNull(ctorEntityDTO);
        this.service = service;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Iterable<T_DTO>> findAll() {
        ArrayList<T_DTO> result = new ArrayList<T_DTO>();

        for (T entity : this.service.getAll()) {
            result.add(ctorEntityDTO.get().map(entity));
        }

        return new ResponseEntity<Iterable<T_DTO>>(result, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<T_DTO> getOne(@PathVariable("id") T_ID id) {
        T entity = this.service.findById(id).orElse(null);

        if (entity == null) {
            return new ResponseEntity<T_DTO>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<T_DTO>(ctorEntityDTO.get().map(entity), HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<T_DTO> create(@RequestBody T_DTO newProvideEntity) {

        T newCreatedEntity = ctorEntity.get().newInstance(newProvideEntity);

        T entity = this.service.create(newCreatedEntity);

        return new ResponseEntity<T_DTO>(ctorEntityDTO.get().map(entity), HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.PUT)
    public ResponseEntity<T_DTO> update(@RequestBody UpdateEntityDTO<T_ID, T_DTO> updateEntityDTO) {

        T_ID id = updateEntityDTO.getUpdateEntityID();
        T_DTO updatedEntity = updateEntityDTO.getUpdateEntity();

        T entity = this.service.findById(id).orElse(null);

        if (entity == null) {
            return new ResponseEntity<T_DTO>(HttpStatus.NOT_FOUND);
        }

        entity.update(updatedEntity);

        entity = this.service.update(entity, entity.getId());

        return new ResponseEntity<T_DTO>(ctorEntityDTO.get().map(entity), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<T_DTO> delete(@PathVariable("id") T_ID id) {
        T entity = this.service.findById(id).orElse(null);

        if (entity == null) {
            return new ResponseEntity<T_DTO>(HttpStatus.NOT_FOUND);
        }

        this.service.delete(id);

        return new ResponseEntity<T_DTO>(HttpStatus.OK);
    }
}
