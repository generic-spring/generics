package generic.spring.generics.simple;

import generic.spring.generics.base.BaseEntity;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;
import java.util.Optional;

public class SimpleGenericEntityService<T extends BaseEntity<T_ID>, T_ID extends Serializable> {

    protected CrudRepository<T, T_ID> repository;

    public SimpleGenericEntityService(CrudRepository<T, T_ID> repository) {
        this.repository = repository;
    }

    public Iterable<T> getAll() {
        return this.repository.findAll();
    }

    public Optional<T> findById(T_ID id) {
        return this.repository.findById(id);
    }

    public T create(T entity) {
        return this.repository.save(entity);
    }

    public T update(T entity, T_ID id) {
        if (entity != null && this.findById(id).isPresent()) {
            return this.repository.save(entity);
        }
        return null;
    }

    public T update(T entity) {
        if (entity != null && this.findById(entity.getId()).isPresent()) {
            return this.repository.save(entity);
        }
        return null;
    }

    public void delete(T entity) {
        this.repository.delete(entity);
    }

    public void delete(T_ID id) {
        this.repository.deleteById(id);
    }
}
