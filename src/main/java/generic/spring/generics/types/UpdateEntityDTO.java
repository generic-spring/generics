package generic.spring.generics.types;

public class UpdateEntityDTO<ID_T, T> {

    private T updateEntity;
    private ID_T updateEntityID;

    public UpdateEntityDTO() {
    }

    public UpdateEntityDTO(T updateEntity, ID_T updateEntityID) {
        this.updateEntity = updateEntity;
        this.updateEntityID = updateEntityID;
    }

    public static <ID_T, T> UpdateEntityDTO<ID_T, T> get() {
        return new UpdateEntityDTO<ID_T, T>();
    }

    public T getUpdateEntity() {
        return updateEntity;
    }

    public void setUpdateEntity(T updateEntity) {
        this.updateEntity = updateEntity;
    }

    public ID_T getUpdateEntityID() {
        return updateEntityID;
    }

    public void setUpdateEntityID(ID_T updateEntityID) {
        this.updateEntityID = updateEntityID;
    }

    public UpdateEntityDTO<ID_T, T> map(UpdateEntityDTO<ID_T, T> e) {
        this.updateEntity = e.getUpdateEntity();
        this.updateEntityID = e.getUpdateEntityID();
        return this;
    }
}