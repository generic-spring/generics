package generic.spring.generics.types;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class DeleteEntitiesDTO<T_ID> {
    List<T_ID> ids;
}
