package generic.spring.generics.types;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class UpdateEntitiesDTO<ID_T, T> {
    List<UpdateEntityDTO<ID_T, T>> entities;

    public UpdateEntityDTO<ID_T, T> get(Integer pos) {
        return this.entities.get(pos);
    }
}
