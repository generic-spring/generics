package generic.spring.generics.types;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class NewEntitiesDTO<T> {
    List<T> entities;

    public T get(Integer pos) {
        return this.entities.get(pos);
    }
}
