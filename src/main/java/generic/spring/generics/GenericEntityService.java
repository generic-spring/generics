package generic.spring.generics;

import generic.spring.generics.base.BaseEntity;
import generic.spring.generics.base.BaseRepository;
import generic.spring.generics.utils.ObjectUtil;
import generic.spring.generics.utils.StringUtil;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.Serializable;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public abstract class GenericEntityService<
        T extends BaseEntity<T_ID> & Mapper<T_DTO, T>,
        T_DTO,
        T_ID extends Serializable> {

    protected final Supplier<T> ctorEntity;
    protected BaseRepository<T, T_ID> repository;

    public GenericEntityService(BaseRepository<T, T_ID> repository, Supplier<T> ctorEntity) {
        this.repository = repository;
        this.ctorEntity = ctorEntity;
    }

    // GET - getAll, getOne, search, filter ----------------------------------------------------------------------------
    // Get all entity
    public Iterable<T> getAll() {
        return this.repository.findAll();
    }

    // Get one entity
    public Optional<T> findById(T_ID id) {
        return this.repository.findById(id);
    }

    // Search over entity's
    public Iterable<T> search(String key) {
        List<String> searchTerms = List.of(key.split("\\s+"));

        Map<T, Integer> weighted = new HashMap<>();

        for (T entry : this.repository.findAll()) {

            String entityStr = StringUtil.clean(ObjectUtil.getValues(entry));
            int val = 0;

            for (String s : searchTerms) {
                if (entityStr.toLowerCase().contains(s.toLowerCase())) {
                    if (entityStr.equals(s)) {
                        val += 2;
                    } else {
                        val += 1;
                    }
                }
            }

            if (val > 0) {
                weighted.put(entry, val);
            }
        }

        List<T> sorted = weighted.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        Collections.reverse(sorted);
        return sorted;
    }

    // Filter over entity's'
    public Iterable<T> filter(@RequestParam Map<String, String> params, Integer sort) {
        System.out.println("Filtering");
        ExampleMatcher matcher = ExampleMatcher
                .matchingAll()
                .withIgnoreCase()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
        T n;

        try {
            n = ctorEntity.get().map(params);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return null;
        }


        var exampleE = Example.of(n, matcher);

        List<T> res = this.repository.findAll(exampleE);

        return res;
    }

    // POST - create(all), create(one), --------------------------------------------------------------------------------
    // Create one entity
    public T create(T entity) {
        return this.repository.save(entity);
    }

    // Create all entities
    public Iterable<T> create(List<T> entities) {
        return this.repository.saveAll(entities);
    }


    // PUT - update(one), ----------------------------------------------------------------------------------------------
    // Update one entity
    public T update(T entity, T_ID id) {
        if (entity != null && this.findById(id).isPresent()) {
            return this.repository.save(entity);
        }
        return null;
    }

    // Update one entity
    public T update(T entity) {
        if (entity != null && this.findById(entity.getId()).isPresent()) {
            return this.repository.save(entity);
        }
        return null;
    }

    // Update all entities
    public Iterable<T> update(List<T> entities) {
//
        return this.repository.saveAllAndFlush(entities);
    }


    // DELETE - delete(one), delete(all) -------------------------------------------------------------------------------
    // Delete one entity
    public void delete(T entity) {
        this.repository.delete(entity);
    }

    // Delete one entity by its id
    public void delete(T_ID id) {
        this.repository.deleteById(id);
    }

    // Delete all entities
    public void delete(List<T> entities) {
        this.repository.deleteAll(entities);
    }
}
