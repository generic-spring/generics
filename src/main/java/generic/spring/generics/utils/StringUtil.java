package generic.spring.generics.utils;

import java.util.List;

public class StringUtil {
    public static String clean(List<String> list) {
        StringBuilder res = new StringBuilder();
        for (String s : list) {
            res.append(" ").append(s);
        }
        return res.toString();
    }
}
