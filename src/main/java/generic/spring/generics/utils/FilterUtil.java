package generic.spring.generics.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FilterUtil {
    static <K, V> Map<K, V> filterByValue(Map<K, V> map, Predicate<V> predicate) {
        return map.entrySet()
                .stream()
                .filter(entry -> predicate.test(entry.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public static <T> List<T> sortBucketByValueReversedInPlace(Map<T, Integer> map) {
        List<T> sorted = new ArrayList<>();
        List<List<T>> buckets = new ArrayList<>();

        for (var entry : map.entrySet()) {

            buckets.get(entry.getValue()).add(entry.getKey());
        }

        return sorted;
    }

}
