package generic.spring.generics.utils;

import org.springframework.stereotype.Service;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ObjectUtil {
    public static List<String> getValues(Object obj) {
        List<String> values = new ArrayList<String>();
        try {
            for (PropertyDescriptor propertyDescriptor :
                    Introspector.getBeanInfo(obj.getClass(), Object.class).getPropertyDescriptors()) {

                // propertyEditor.getReadMethod() exposes the getter
                // btw, this may be null if you have a write-only property
                Object res = propertyDescriptor.getReadMethod().invoke(obj);

                if (res != null) {
                    if (res.getClass().isPrimitive()) {
                        values.add(res.toString());
                    } else if (res.getClass().isArray()) {
                        values.add(res.toString());
                    } else if (res.getClass().getPackage().toString().contains("com.example.demo")) {
                        values.addAll(ObjectUtil.getValues(res));
//                        values.add("Prc");
                    } else {
                        try {
                            String v = res.toString();
                            values.add(v);
                        } catch (Exception ignored) {

                        }
                    }
                }
            }
        } catch (IntrospectionException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return values;
    }

    public static <T> Map<String, Field> getFields(T t) {
        Map<String, Field> fields = new HashMap<String, Field>();

        Class<?> clazz = t.getClass();

        while (clazz != Object.class) {

            fields.putAll(Stream.of(clazz.getDeclaredFields()).collect(Collectors.toMap(Field::getName, item -> item)));

            clazz = clazz.getSuperclass();
        }

        return fields;
    }

}
