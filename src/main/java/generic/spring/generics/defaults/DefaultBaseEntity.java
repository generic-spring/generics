package generic.spring.generics.defaults;

import generic.spring.generics.base.BaseEntity;
import jakarta.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;


@Getter
@Setter
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@ToString(callSuper = true)
@MappedSuperclass
public abstract class DefaultBaseEntity extends BaseEntity<Long> {
}
