package generic.spring.generics;

import generic.spring.generics.base.BaseEntity;
import generic.spring.generics.types.DeleteEntitiesDTO;
import generic.spring.generics.types.NewEntitiesDTO;
import generic.spring.generics.types.UpdateEntitiesDTO;
import generic.spring.generics.types.UpdateEntityDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

public abstract class GenericEntityControllerImpl<
        T extends BaseEntity<T_ID> & Mapper<T_DTO, T>,
        T_DTO extends Mapper<T, T_DTO>,
        T_ID extends Serializable
        > extends GenericEntityControllerAbs<T, T_DTO, T_ID> {

    private final GenericEntityService<T, T_DTO, T_ID> service;
    private final Supplier<T> ctorEntity;
    private final Supplier<T_DTO> ctorEntityDTO;

    public GenericEntityControllerImpl(Supplier<T> ctorEntity,
                                       Supplier<T_DTO> ctorEntityDTO,
                                       GenericEntityService<T, T_DTO, T_ID> service) {
        this.ctorEntity = Objects.requireNonNull(ctorEntity);
        this.ctorEntityDTO = Objects.requireNonNull(ctorEntityDTO);
        this.service = service;
    }

    // GET - getAll, getOne, search, filter ----------------------------------------------------------------------------
    // Get all entity
    @Override
    public ResponseEntity<Iterable<T_DTO>> findAll() {
        ArrayList<T_DTO> result = new ArrayList<T_DTO>();

        for (T entity : this.service.getAll()) {
            result.add(ctorEntityDTO.get().map(entity));
        }
//        T e = this.service.getAll().iterator().next();
//        result.add(ctorEntityDTO.get().map(e));

        System.out.println("result: " + result);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    // Get one entity
    @Override
    public ResponseEntity<T_DTO> getOne(@PathVariable("id") T_ID id) {
        T entity = this.service.findById(id).orElse(null);

        if (entity == null) {
            return new ResponseEntity<T_DTO>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<T_DTO>(ctorEntityDTO.get().map(entity), HttpStatus.OK);
    }

    // Search over entity's
    @Override
    public ResponseEntity<Iterable<T_DTO>> search(@RequestParam(name = "key", defaultValue = "") String key,
                                                  @RequestParam(name = "mode", defaultValue = "1", required = false) Integer mode) {

        List<T> result = (List<T>) this.service.search(key);

        return new ResponseEntity<Iterable<T_DTO>>(result.stream().map(e -> ctorEntityDTO.get().map(e)).toList(), HttpStatus.OK);
    }

    // Filter over entity's
    @Override
    public ResponseEntity<Iterable<T_DTO>> filter(
            @RequestParam(name = "_sort", defaultValue = "1") Integer sort,
            @RequestParam Map<String, String> params) {
        params.remove("_sort", sort.toString()); // removing unnecessary sort(redundant)

        return new ResponseEntity<Iterable<T_DTO>>(
                ((List<T>) this.service.filter(params, sort))
                        .stream()
                        .map(e -> ctorEntityDTO.get().map(e))
                        .toList(), HttpStatus.OK);

    }


    // POST - create(all), create(one), --------------------------------------------------------------------------------
    // Create all entities
    @Override
    public ResponseEntity<Iterable<T_DTO>> createAll(@RequestBody NewEntitiesDTO<T_DTO> newProvidedEntity) {

        List<T> entities = newProvidedEntity.getEntities().stream().map(e -> ctorEntity.get().map(e)).toList();

        List<T> freshlyCreatedEntities = (List<T>) this.service.create(entities);

        return new ResponseEntity<>(freshlyCreatedEntities.stream().map(e -> ctorEntityDTO.get().map(e)).toList(), HttpStatus.OK);
    }

    // Create one entity
    @Override
    public ResponseEntity<T_DTO> create(@RequestBody T_DTO newProvideEntity) {

        T newCreatedEntity = ctorEntity.get().newInstance(newProvideEntity);

        T entity = this.service.create(newCreatedEntity);

        return new ResponseEntity<T_DTO>(ctorEntityDTO.get().map(entity), HttpStatus.OK);
    }


    // PUT - update(one), ----------------------------------------------------------------------------------------------
    // Update all entities
    @Override
    public ResponseEntity<Iterable<T_DTO>> updateAll(@RequestBody UpdateEntitiesDTO<T_ID, T_DTO> updateEntitiesDTO) {
        List<T> entities = new ArrayList<>();

        for (UpdateEntityDTO<T_ID, T_DTO> updateEntityDTO : updateEntitiesDTO.getEntities()) {
            T_ID id = updateEntityDTO.getUpdateEntityID();
            T_DTO updatedEntity = updateEntityDTO.getUpdateEntity();

            T entity = this.service.findById(id).orElse(null);

            if (entity == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            entities.add(entity.update(updatedEntity));
        }

        List<T> freshlyUpdatedEntity = (List<T>) this.service.update(entities);

        return new ResponseEntity<>(freshlyUpdatedEntity.stream().map(e -> ctorEntityDTO.get().map(e)).toList(), HttpStatus.OK);
    }

    // Update one entity
    @Override
    public ResponseEntity<T_DTO> update(@RequestBody UpdateEntityDTO<T_ID, T_DTO> updateEntityDTO) {

        T_ID id = updateEntityDTO.getUpdateEntityID();
        T_DTO updatedEntity = updateEntityDTO.getUpdateEntity();

        System.out.println("Updating entity " + id + " with " + updatedEntity);

        T entity = this.service.findById(id).orElse(null);

        if (entity == null) {
            return new ResponseEntity<T_DTO>(HttpStatus.NOT_FOUND);
        }

        entity = entity.update(updatedEntity);

//        System.out.println("Updated entity : ");

        T freshlyUpdatedEntity = this.service.update(entity, entity.getId());

        return new ResponseEntity<T_DTO>(ctorEntityDTO.get().map(freshlyUpdatedEntity), HttpStatus.CREATED);
    }


    // DELETE - delete(one), delete(all) -------------------------------------------------------------------------------
    // Delete all entities
    @Override
    public ResponseEntity<T_DTO> deleteAll(@RequestBody DeleteEntitiesDTO<T_ID> deleteEntitiesDTO) {
        List<T> entities = new ArrayList<>();

        for (T_ID id : deleteEntitiesDTO.getIds()) {
            T entity = this.service.findById(id).orElse(null);

            if (entity == null) {
                return new ResponseEntity<T_DTO>(HttpStatus.NOT_FOUND);
            }

            entities.add(entity);
        }
        this.service.delete(entities);

        return new ResponseEntity<T_DTO>(HttpStatus.OK);
    }

    // Delete one entity
    @Override
    public ResponseEntity<T_DTO> delete(@PathVariable("id") T_ID id) {
        T entity = this.service.findById(id).orElse(null);

        if (entity == null) {
            return new ResponseEntity<T_DTO>(HttpStatus.NOT_FOUND);
        }

        this.service.delete(id);

        return new ResponseEntity<T_DTO>(HttpStatus.OK);
    }


}
