package generic.spring.generics;

import com.fasterxml.jackson.databind.ObjectMapper;
import generic.spring.generics.base.BaseEntity;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

// Dynamic casting ?
// Mapper checker
// Moze se definisati return value, kao Object i kasnije reimplementirati, posto je svaka klasa tipa Object i zadovalja intreface

/**
 * Interfejs koji pruza mogucnost mapiranja objekata,
 * Korisit se prvesntveno u slucjau potrebe mapiranja objekat @Entity i njegovog DTO-a, kao i obrnuto
 *
 * @param <T>      Tip objekata mapiranja
 * @param <T_Self> Tip objekta nad kojim se vrsi mapiranje, neophodan nam je i ovaj tim da bi smo znali koja je povratna vrednost metoda,
 *                 zbog potencijalne potrebe nadovezivanja metoda, kao i zbog mogucnosit kreiranja objekta "inplace"
 */
public interface Mapper<T, T_Self> {
    ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
    Batman batman = new Batman();
//    default Set<Field> getIgnoredFields(){
//        return new HashSet<>();
//    }
//
//    default Set<Field> getRecusiveField(){
//        return new HashSet<>();
//    }

    /**
     * Korisiti se u slucajvima gde je potrebno mapirati(kopirati) sve vrednosti jednog objekta na drugi.
     * Npr. mapiranje entiteta na njegov DTO, gde nam je potrebno postaviti sve vrednosti iz entieta na DTO.
     * <p>
     * Mapira vrednosti objekta(T) na (sebe)objekat(T_Self).
     *
     * @param entity Objekat(T) koji se kopira
     * @return Vraca se objekat na koji se kopira, da bi se ova operacija mogla nadovezati, ili da bi kreiranje objekat moglo biti "inplace"
     */
    default T_Self map(T entity) {
        try {
            Set<Class<?>> list = new HashSet<>();
            entity = batman.resolve(entity, list);
            String value = mapper.writeValueAsString(entity);
            System.out.println("Mapping value" + value);

            return (T_Self) mapper.readValue(value, this.getClass());

        } catch (Exception e) {
            return null;
        }

    }

    default T_Self map(Map<String, String> parms) {
        try {
            return (T_Self) mapper.readValue(mapper.writeValueAsString(parms), this.getClass());

        } catch (Exception e) {
            System.out.println("Ups greska");
            return null;
        }
    }

    /**
     * Koristi se u slucajevima kada je potrebno namestiti nov "cist" entitet, tako da se mapiranje izvrsava za sve vrednosti osim za id.
     * <p>
     * Mapira vrednosti objekta(T) na (sebe)objekat(T_Self), SA IZUZETKOM ID, vrednost ID ostaje przna.
     *
     * @param entity Objekat(T) koji se kopira
     * @return Vraca se objekat na koji se kopira, da bi se ova operacija mogla nadovezati, ili da bi kreiranje objekat moglo biti "inplace"
     */
    default T_Self newInstance(T entity) {
        try {

            Field id = entity.getClass().getDeclaredField("id");
            id.setAccessible(true);
            id.set(entity, null);

            Set<Class<?>> list = new HashSet<>();
            entity = batman.resolve(entity, list);

            String value = mapper.writeValueAsString(entity);

            return (T_Self) mapper.readValue(value, this.getClass());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Koristi se u slucajevima kada je potrebno izmeniti vec postojaci objekat, mapiraju se sve vrednosti koje je moguce menjati.
     * <p>
     * Mapira vrednosti objekta(T) na postojaci (sebe)objekat(T_Self),
     * stim da treba izostaviti mapiranje vrednosti koje ne bi trebalo da budu moguce izmeniti(npr. ID).
     *
     * @param entity Objekat(T) koji se kopira
     * @return Vraca se objekat na koji se kopira, da bi se ova operacija mogla nadovezati, ili da bi kreiranje objekat moglo biti "inplace"
     */
    default T_Self update(T entity) {
        try {
            Field self_id;
            if (this.getClass().getSuperclass() == BaseEntity.class) {
                self_id = this.getClass().getSuperclass().getDeclaredField("id");
            } else {
                self_id = this.getClass().getDeclaredField("id");
            }
            Field entity_id = entity.getClass().getDeclaredField("id");
            entity_id.setAccessible(true);
            self_id.setAccessible(true);
            entity_id.set(entity, self_id.get(this));

            Set<Class<?>> list = new HashSet<>();
            entity = batman.resolve(entity, list);

            String value = mapper.writeValueAsString(entity);

            T_Self res = (T_Self) mapper.readValue(value, this.getClass());

            return res;

        } catch (Exception e) {
            return null;
        }
    }

}