package generic.spring.generics;


import generic.spring.generics.types.DeleteEntitiesDTO;
import generic.spring.generics.types.NewEntitiesDTO;
import generic.spring.generics.types.UpdateEntitiesDTO;
import generic.spring.generics.types.UpdateEntityDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

public abstract class GenericEntityControllerAbs<T, T_DTO, T_ID> {

    // GET - getAll, getOne, search, filter ----------------------------------------------------------------------------
    // Get all entity
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public abstract ResponseEntity<Iterable<T_DTO>> findAll();

    // Get one entity
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public abstract ResponseEntity<T_DTO> getOne(@PathVariable("id") T_ID id);

    // Search over entity's
    @RequestMapping(path = "/search", method = RequestMethod.GET)
    public abstract ResponseEntity<Iterable<T_DTO>> search(@RequestParam(name = "key", defaultValue = "") String key,
                                                           @RequestParam(name = "mode", defaultValue = "1", required = false) Integer mode); // mode 1 is contains

    // Filter over entity's
    @RequestMapping(path = "/filter", method = RequestMethod.GET)
    public abstract ResponseEntity<Iterable<T_DTO>> filter(
            @RequestParam(name = "sort", defaultValue = "1") Integer sort,
            @RequestParam Map<String, String> params);


    // POST - create(all), create(one), --------------------------------------------------------------------------------
    // Create all entities
    @RequestMapping(path = "/all", method = RequestMethod.POST)
    public abstract ResponseEntity<Iterable<T_DTO>> createAll(@RequestBody NewEntitiesDTO<T_DTO> newProvidedEntity);

    // Create one entity
    @RequestMapping(path = "", method = RequestMethod.POST)
    public abstract ResponseEntity<T_DTO> create(@RequestBody T_DTO newProvideEntity);


    // PUT - update(one), ----------------------------------------------------------------------------------------------
    // Update all entities
    @RequestMapping(path = "/all", method = RequestMethod.PUT)
    public abstract ResponseEntity<Iterable<T_DTO>> updateAll(@RequestBody UpdateEntitiesDTO<T_ID, T_DTO> updateEntitiesDTO);

    // Update one entity
    @RequestMapping(path = "", method = RequestMethod.PUT)
    public abstract ResponseEntity<T_DTO> update(@RequestBody UpdateEntityDTO<T_ID, T_DTO> updateEntityDTO);


    // DELETE - delete(one), delete(all) -------------------------------------------------------------------------------
    // Delete one entity
    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public abstract ResponseEntity<T_DTO> delete(@PathVariable("id") T_ID id);

    // Delete all entities
    @RequestMapping(path = "/all", method = RequestMethod.DELETE)
    public abstract ResponseEntity<T_DTO> deleteAll(@RequestBody DeleteEntitiesDTO<T_ID> deleteEntitiesDTO);
}
