package generic.spring.generics;

import jakarta.persistence.Entity;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

public class Batman {

    public <T> T resolve(T object, Set<Class<?>> list) {

        list.add(object.getClass());

//        System.out.println("Resolving " + object.getClass() + " for " + list);

        for (Field field : object.getClass().getDeclaredFields()) {
            Class<?> fieldType = field.getType();
//            System.out.println("..Field  " + fieldType + " with name: " + field.getName() + " and value: ");

//            if (fieldType == object.getClass()){
//                System.out.println("....Maskiraj 'sam sebe' Field " + field.getName()  + "  in class " + object.getClass().getName());
//                field.setAccessible(true);
//                try {
//                    field.set(object, null);
//                } catch (IllegalAccessException e) {
//                    throw new RuntimeException(e);
//                }
//                continue;
//            }

            if (list.contains(fieldType)) {
//                System.out.println("....Maskiraj Field " + field.getName() + "  in class " + object.getClass().getName());
                field.setAccessible(true);
                try {
                    field.set(object, null);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
                continue;
            }


            if (fieldType.isAnnotationPresent(Entity.class)) {
//                System.out.println("....Nadjen @Entity " + field.getName() + "  in class " + object.getClass().getName());
                field.setAccessible(true);
                try {
                    Set<Class<?>> _list = new HashSet<>(list);
                    _list.add(fieldType);
                    Object o = this.resolve(field.get(object), _list);
                    field.set(object, o);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
                continue;
            }

            if (field.getType() == Set.class) {
//                if(true){
//                    continue;
//                }
                field.setAccessible(true);

                try {
                    Set c = (Set) field.get(object);
                    if (c == null) {
                        continue;
                    } else {

                        if (c.isEmpty()) {
                            continue;
                        }

                        Object next = c.stream().toArray()[0];

                        if (list.contains(next.getClass())) {
//                            System.out.println("....Nadjen trazenih 'sam sebe' Set<@Entity> " + field.getName() + "  in class " + object.getClass().getName());
                            field.setAccessible(true);
                            field.set(object, null);
                            continue;
                        }


//                        if(next.getClass() == self){
//                            System.out.println("....Nadjen trazenih Set<@Entity> " + field.getName()  + "  in class " + object.getClass().getName());
//                            field.setAccessible(true);
//                            field.set(object, null);
//                            continue;
//                        }

                        if (next.getClass().isAnnotationPresent(Entity.class)) {
//                            System.out.println("....Nadjen Set<@Entity> " + field.getName() + "  in class " + object.getClass().getName());

                            Set<Object> c2 = new HashSet<>();
                            Set<Class<?>> _list = new HashSet<>(list);
                            _list.add(next.getClass());
//                            next = this.resolve(next, list);
//                            c2.add(next);
//                            while (c.iterator().hasNext()) {
//                                Object _next = c.iterator().next();
//                                c2.add(this.resolve(_next, list));
//                            }

                            for (Object o : c) {
                                c2.add(this.resolve(o, _list));
                            }


                            field.setAccessible(true);
                            field.set(object, c2);
                            continue;
                        }
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return object;
    }

}
